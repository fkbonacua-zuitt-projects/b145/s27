
	db.fruits.insertMany([
			{	
				"name": "Apple",
				"color": "Red",
				"stock": 20,
				"price": 40,
				"onSale": true,
				"origin": ["Philippines", "US"]
			},
			{	
				"name": "Banana",
				"color": "Yellow",
				"stock": 15,
				"price": 20,
				"onSale": true,
				"origin": ["Philippines", "Ecuador"]
			},
			{	
				"name": "Kiwi",
				"color": "Green",
				"stock": 25,
				"price": 50,
				"onSale": true,
				"origin": ["US", "China"]
			},
			{	
				"name": "Mango",
				"color": "Yellow",
				"stock": 10,
				"price": 120,
				"onSale": false,
				"origin": ["Philippines", "India"]
			}				
		]);

// Aggregation Syntax

	db.collection.aggregate(
		[
			{ <stage 1> },
			{ <stage 2> },
			{ <stage 3> }
		]
		);

// Stages

	//  $match
		// Filters the documents to pass only the documents that match the specified condition(s) to the next pipeline stage.
	// syntax:
		{ $match: {query} }

			// mini activity
				// group together documents that fruits are currently on sale

	db.fruits.aggregate(
			[
				{
					$match: {
						"onSale": true
					}
				}



			]
		);

	//  $count
		// Returns a count of the number of documents at this stage of the aggregation pipeline.
			// syntax:
				{ $count: <string> }

		// mini activity
			// after grouping documents according to fruits currently on sale, count how many documents were returned by assigning it to fruitCount name

	db.fruits.aggregate(
			[
				{
					$match: {
						"onSale": true
					}
				},
				{
					$count: "fruitCount"
				}



			]
		);

	// $group
		// Groups input documents by a specified identifier expression and applies the accumulator expression(s), if specified, to each group. Consumes all input documents and outputs one document per each distinct group. The output documents only contain the identifier field and, if specified, accumulated fields.
			// syntax:
			{
				  $group:
				    {
				      _id: <expression>, // Group By Expression
				      <field1>: { <accumulator1> : <expression1> },
				      ...
				    }
				 }

		// mini activity
			// after grouping documents according to fruits currently on sale, group documents according to null, and get the total sum of the stock field then assign it to "total" field (use $sum accumulator operator)
db.fruits.aggregate(
	[
		{$match: {"onSale": true} },
		{$group: 
			{
				_id: null,
				"total": {$sum: "$stock" }
			} 
		}
	]
);

	// $project
			// Reshapes each document in the stream, such as by adding new fields or removing existing fields.

			// syntax:
			{ $project: { <specification(s)> } }

			// mini activity
				// group together fruits that has more than 10 stocks and then exclude id in the returning document
	db.fruits.aggregate(
		[
			{
				$match: {"stock": {$gt: 10}}
			},
			{
				$project: {"_id": 0}
			}
		]
	);

	// $sort
		//Reorders the document stream by a specified sort key. Only the order changes; the documents remain unmodified.

		// syntax
			{ $sort: { <field1>: <sort order>, <field2>: <sort order> ... } }

		// sort returned documents in ascending order
	db.fruits.aggregate(
		[
			{
				$match:{"stock":{$gt:10}}
			},
			{
				$project:{"_id":0}
			},
			// ascending order
			// {
			// 	$sort:{"price":1}
			// }
			{ $sort: {"price": -1}}
]
);

	// $unwind
		// syntax:
			{ $unwind: <field path> }


	db.fruits.aggregate(
		[
			{ $unwind: "$origin"}	
		]
);

	// mini activity
		// group together same origin and count how many documents are there in each origin

	db.fruits.aggregate(
		[
			{ $unwind: "$origin"},
			{ 
				$group: {_id: "$origin", kinds: { $sum: 1}}
			}	
		]
);