// answer #1
	db.fruits.aggregate(
			[
				{
					$match: {
						"onSale": true
					}
				},
				{
					$count: "fruitCount"
				}
			]
		);

// answer #2
	db.fruits.aggregate(
			[
				{
					$match: {
						"stock": {$gte:20}
					}
				},
				{
					$count: "enoughStock"
				}
			]
		);

// answer #3
	db.fruits.aggregate(
			[
				{
					$match: {
						"onSale": true
					}
				},
				{
					$group: {
						_id: null,
						"avg_price": {$avg: "$price"}
					}
				}
			]
		);

// answer #4
	db.fruits.aggregate(
			[
				{
					$group: {
						_id: null,
						"max_price": {$max: "$price"}
					}
				}
			]
		);
// answer#5
	db.fruits.aggregate(
			[
				{
					$group: {
						_id: null,
						"min_price": {$min: "$price"}
					}
				}
			]
		);